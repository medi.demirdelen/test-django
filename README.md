Le projet django pour traiter les commandes via l'API XML.
L'app orders contient une commande "seed chemin_du_fichier" pour importer les données via le fichier XML dans le modèle. (5 champs sont supportés)
L'app contient également une vue très primaire pour afficher les données du modèle, et des champs pour rechercher via des requêtes AJAX mais celles-ci ne fonctionnent pas.
Je n'ai pas encore rajouté de Bootstrap.