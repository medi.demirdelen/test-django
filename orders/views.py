from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django import forms
from django.views.generic.base import View, TemplateView
from orders.models import Order
import sys

# Create your views here.

class SearchField(View):
    def get(self, request, *args, **kwargs):
        print(kwargs, file=sys.stderr)
        results = Order.objects.filter(currency = kwargs['search_field'])

        html = render(request, "orderlist.html", {'search_field': results})
        return HttpResponse(html)
        #la classe qui fait le traitement ajax des recherches sur les champs (ici, j'ai juste mis pour le dernier champ pour essayer) ceci dit, ne fonctionne pas comme prévu

class OrdersList(TemplateView):
    template_name = "orderlist.html"

    def get_context_data(self, **kwargs):
        context = super(OrdersList, self).get_context_data(**kwargs)
        orders = Order.objects.all()
        context['orders'] = []
        i = 0
        for order in orders:
            context['orders'].append(i)
            context['orders'][i] = {}
            context['orders'][i]['id'] = i+1
            context['orders'][i]['market_place'] = order.market_place
            context['orders'][i]['purchase_date'] = order.purchase_date
            context['orders'][i]['purchase_time'] = order.purchase_time
            context['orders'][i]['amount'] = order.amount
            context['orders'][i]['currency'] = order.currency
            i = i + 1
        return context