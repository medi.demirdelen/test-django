from django.core.management.base import BaseCommand, CommandError
from orders.models import Order
import xml.etree.ElementTree as ET

#la commande pour insérer les données dans le modèle, la syntaxe est seed "chemin du fichier xml"

class Command(BaseCommand):
    help = 'Fills orders database from xml api'

    def add_arguments(self, parser):
        parser.add_argument('xml_path', type=str, help='Path of the xml file to import')

    def handle(self, *args, **kwargs):
        list_orders = ET.parse(kwargs['xml_path']).getroot()[1]
        for order in list_orders:
            o = Order()
            o.market_place = order[0].text
            o.purchase_date = order[7].text #peut être null
            o.purchase_time = order[8].text #peut être null
            o.amount = order[9].text
            o.currency = order[14].text
            o.save()