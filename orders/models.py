from django.db import models
# Create your models here.

class Order(models.Model):
    market_place = models.CharField(max_length=50)
    purchase_date = models.DateField(null=True)
    purchase_time = models.TimeField(null=True)
    amount = models.FloatField()
    currency = models.CharField(max_length=3)